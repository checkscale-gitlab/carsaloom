FROM openjdk:11.0.8-jre-slim
COPY build/carSalon-0.0.1.jar carSalon.jar
ENTRYPOINT ["java", "-jar", "carSalon.jar"]