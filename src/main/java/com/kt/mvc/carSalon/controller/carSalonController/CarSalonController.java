package com.kt.mvc.carSalon.controller.carSalonController;

import com.kt.mvc.carSalon.controller.carCreatorController.CarCreatorControllerMVC;
import com.kt.mvc.carSalon.controller.printerController.PrinterMVC;
import com.kt.mvc.carSalon.controller.scannerController.ScannerMVC;
import com.kt.mvc.carSalon.model.Person;

public class CarSalonController implements CarSalonControllerMVC.Controller {

    private CarCreatorControllerMVC.Controller carCreatorControllerMVC;
    private ScannerMVC.Controller scannerController;
    private Person person;
    private PrinterMVC.Controller printerController;

    public CarSalonController(CarCreatorControllerMVC.Controller carCreatorController,
                              ScannerMVC.Controller scannerController,
                              Person person,
                              PrinterMVC.Controller printerController) {
        this.carCreatorControllerMVC = carCreatorController;
        this.scannerController = scannerController;
        this.person = person;
        this.printerController = printerController;

    }

    public void configure() {

        printerController.println("Wybierz opcję");
        printerController.println("1. Wybór koloru");
        printerController.println("2. Wybór paliwa");
        printerController.println("3. Wybór marki");
        printerController.println("4. Wybór nadwozia");
        printerController.println("5. Wybór tapicerki");
        printerController.println("6. Koniec");
        printerController.println("7. Pokaż stan portfela");
        printerController.println("8. Pokaż auto");

        int choice = scannerController.pickOption();

        switch (choice) {
            case 1:
                carCreatorControllerMVC.pickColor();
                configure();
                break;
            case 2:
                carCreatorControllerMVC.pickFuelType();
                configure();
                break;
            case 3:
                carCreatorControllerMVC.pickBrand();
                configure();
                break;
            case 4:
                carCreatorControllerMVC.pickBodyType();
                configure();
                break;
            case 5:
                carCreatorControllerMVC.pickUpholstery();
                configure();
                break;
            case 6:
                break;
            case 7:
                printerController.println(person.getCurrentWallet());
                configure();
                break;
            case 8:
                printerController.println(carCreatorControllerMVC.returnCar());
                configure();
                break;
            default:
                configure();
                break;
        }
    }
}
