package com.kt.mvc.carSalon.controller.printerController;

public interface PrinterMVC {
    interface Controller {

        void println(Object object);

    }
}
