package com.kt.mvc.carSalon.model;

import com.kt.mvc.carSalon.controller.printerController.PrinterController;
import com.kt.mvc.carSalon.controller.carCreatorController.MenuInterface;
import com.kt.mvc.carSalon.controller.printerController.PrinterMVC;


public enum BodyType {
    SEDAN(0),
    NONE (0),
    PICKUP(2000),
    HATCHBACK(1000),
    KOMBI(1000);

    private int price;

    BodyType(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void checkPickedBodyType(Person person, Car car, MenuInterface menuInterface, PrinterMVC.Controller printer) {
        if (person.getCurrentWallet() + car.getBodyType().getPrice() >= this.getPrice()) {
            person.addMoney(car.getBodyType().getPrice());
            car.setBodyType(this);
            person.subtractMoney(this.getPrice());
        } else {
            printer.println("Not enough money!");
            menuInterface.goToBodyTypePick();
        }
    }
}

