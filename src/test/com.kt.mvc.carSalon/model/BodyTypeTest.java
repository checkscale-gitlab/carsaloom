package com.kt.mvc.carSalon.model;

import com.kt.mvc.carSalon.controller.carCreatorController.MenuInterface;
import com.kt.mvc.carSalon.controller.printerController.PrinterController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class BodyTypeTest {

    Person person;
    Person personPoor;
    Car car;
    MenuInterface menuInterface;
    PrinterController printerController;

    @BeforeEach
    public void setup(){
        person = new Person();
        personPoor = new Person();
        personPoor.addMoney(0);
        person.addMoney(1000000);

        car = new Car();
        car.setBodyType(BodyType.NONE);
        car.setBrand(Brand.NONE);
        car.setColour(Colour.NONE);
        car.setFuelType(FuelType.NONE);
        car.setUpholstery(Upholstery.NONE);

        menuInterface = mock(MenuInterface.class);
        printerController = mock(PrinterController.class);
    }

    @ParameterizedTest
    @EnumSource(value = BodyType.class, names = {"PICKUP", "HATCHBACK", "KOMBI"})
    public void shouldNotAllowToSelectOptionWhenPersonHaveNotEnoughtMoney(BodyType bodyType) {
        //when
        bodyType.checkPickedBodyType(personPoor, car, menuInterface, printerController);
        //then
        assertAll(
                () -> verify(menuInterface).goToBodyTypePick(),
                () -> verify(printerController).println(anyString())
         );
    }

    @ParameterizedTest
    @EnumSource(value = BodyType.class, names = {"PICKUP", "HATCHBACK", "KOMBI"})
    public void shouldAllowToSelectOptionWhenPersonHaveEnoughtMoney(BodyType bodyType) {
        //when
        bodyType.checkPickedBodyType(person, car, menuInterface, printerController);
        //then
        assertAll(
                () -> assertEquals(person.getCurrentWallet(), 1000000 - bodyType.getPrice()),
                () -> assertEquals(car.getBodyType(), bodyType),
                () -> verifyNoMoreInteractions(printerController),
                () -> verifyNoMoreInteractions(menuInterface)
         );
    }

    @ParameterizedTest
    @EnumSource(value = BodyType.class, names = {"SEDAN"})
    public void shouldAllowToSelectFreeOptionWhenPersonSelectIt(BodyType bodyType) {
        //when
        bodyType.checkPickedBodyType(personPoor, car, menuInterface, printerController);
        bodyType.checkPickedBodyType(person, car, menuInterface, printerController);
        //then
        assertAll(
                () -> assertEquals(person.getCurrentWallet(), 1000000 - bodyType.getPrice()),
                () -> assertEquals(car.getBodyType(), bodyType),
                () -> verifyNoMoreInteractions(printerController),
                () -> verifyNoMoreInteractions(menuInterface)
        );
    }
}